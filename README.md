# capistrano-configuration

This repository contains the deployment configuration and custom task for Gosign.


## Relevant tasks

* `local:setup` to locally setup the project structure
* `local:compass` to watch the configured Compass/SCSS folder


## Initial installation

If you'd like to update a project (using Capistrano 2) to use the new Capistrano 3 configuration, follow these steps:

1. Delete the `Capfile` from your project's root directory.
2.


In addition you need to make some changes in `project.rb` and all the configured Capistrano stages:

1. In `project.rb`:
    * Remove the `set :default_stage` directive
    * Change `set :repository` to `set :repo_url`
    * If it does not exist yet, add `set :enable_compass, true`

1. All configured stages should be deleted and re-created based on the template in `capistrano/_examples/stages/staging.rb`

1. Run `./go [insert stage here] remote:uname` to see if the installation worked. It should report something similar to:

    `Linux capistrano 3.11.0-12-generic #19-Ubuntu SMP Wed Oct 9 16:20:46 UTC 2013 x86_64 x86_64 x86_64 GNU/Linux`



## Automatically running unit tests

In order to automatically run unit-tests on deploy, set the following configuration in your `project.rb` or stage config file:

    # Run these unit tests on the remote server
    set :run_unittests, ["typo3conf/ext/go_mobilecontentfilter"]

As the elements of the array, specify the extension for which you want to run unit-tests (you can also specify unit test files directly).


## Troubleshooting

* The configuration isn't updating, or I'm getting a "file not found" error.
    * Try removing the file `config/capistrano/.CURRENT_TAG`. This should force a configuration update.

#### The following (or a similar) error is displayed:

    Don't know how to build task 'deploy:setup'
    /Users/.../lib/capistrano/dsl/task_enhancements.rb:5:in `before'
    /Users/lucas/_dev/_mamp/default6-2/config/capistrano/gosign/tasks/setup.rb:41:in `top (required)'
    /Users/.../lib/capistrano/application.rb:82:in `load_imports'
    /Users/.../lib/capistrano/application.rb:15:in `run'
    /Users/.../bin/cap:3:in `top (required)'
    /Users/.../cap:23:in `load'
    /Users/.../cap:23:in `<main>'
    /Users/.../ruby_executable_hooks:15:in `eval'
    /Users/.../ruby_executable_hooks:15:in `<main>'

Delete your `config/capistrano/gosign` directory and try again.
