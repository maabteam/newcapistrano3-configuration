class Gosign
  module Config

    def loadConfig

# After you have configured capistrano, you canuncomment or remove the
# "abort()"-call. Note that you also need to configure the stages located
# in config/deploy/.
abort("Please configure capistrano first!")

# Default permissions which are set on each deploy (by "deploy:permissions")
# These may be overriden in the stage configuration.
set :filePermissions, 660
set :setDirectoryPermissions, 2770
set :phpshPermissions, 770

# The customer config variable is needed for our server directory structure,
# which looks something like [..]/webs/<customer>/<project>/<stage>/[..]
set :customer, "WICHTIG: Name des Kunden wie in SysCP"

# The application name, which is also used as the applications folder name
# on the remote server and therefore should not contains whitespaces,
# special characters, etc...
set :application, "<project name without whitespaces or special characters>"

# Repository URL.
set :repo_url, "<please enter git repository here>"

# Enabling compass support while search for compass projet directories on deploy, compiling
# the for every deployed release.
set :enable_compass, true

# This is where we will try to run the compass SCSS compiler if enabled
# set :compass_directory, 'Templates'

    end

  end

end
