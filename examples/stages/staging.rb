server '<remote server>', user: 'capistrano', roles: %w{app web}

# Path we want to deploy to. Note that this is not
# htdocs/, but the folder we cloned our "DefaultProjektStruktur"-
# repository to!
set :deploy_to, "/home/kunden/webs/#{fetch :customer}/#{fetch :application}/#{fetch :stage}"

# Owner and group of the deployed files
set :owner, "<owner>"
set :group, "<group>"

# Port to use for scp command
set :scpPort, 46022

# Which branch of our repository do we want to check out?
set :branch, "staging"

# The TYPO3 application context
# Has to be one of 'Production', 'Testing', 'Development'
# Could have one ore more subcontexts, e.g. 'Production/Staging/Stage1'
# Defaults to 'Production' inside of TYPO3
# Is written into a .htaccess file while setup
# Is set to 'Development' for local:setup
set :applicationContext, "Production/Staging"

# In case compass can not be put in the PATH
# Only necessary if compass is actually enabled
# set :compass_path, '/absolute/path/to/command/compass'
