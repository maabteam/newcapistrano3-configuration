require './config/capistrano/gosign/gosign.rb'
require 'capistrano/git'
require './config/capistrano/gosign/lib/submodule_strategy.rb'
require './config/capistrano/gosign/lib/shouting_formatter.rb'
require './config/capistrano/project.rb' # Include Project Configuration

set :format, :shouting
set :log_level, :info

if ENV["VERBOSE"]
  Rake.application.options.trace = true
  Rake.application.options.backtrace = true
  set :log_level, :debug
end

set :default_shell, '/bin/bash'

set :scm, :git
set :deploy_via, :remote_cache
set :use_sudo, false

# Compass should be disabled by default, since not all projects use it.
set :enable_compass, false

# Try to load Compass from PATH by default
set :compass_path, nil

# Enables compass production mode which minifies all CSS files
set :compass_minify, true

# This is where we will try to run the compass SCSS compiler if enabled
set :compass_directory, 'Templates'

# Array of extension paths. Don't run any unit tests by default (empty)
set :run_unittests, []

set :git_strategy, SubmoduleStrategy



# ===============================
# TYPO3 version specific switches
# ===============================

set :cache_locations, ["typo3temp/Cache/*", "typo3temp/autoload/*"]

if Gosign::Util.project_typo3_version & Gosign::Util::PRE_TYPO3_6_0 > 0
  set :cache_locations, [ ]
end



# ==========
# Misc stuff
# ==========

# Due to the fact that we have additional symlinks in our releases directory, the standard
# way of listing the releases doesn't work. The following excludes the symlinks. Without this,
# deploy:rollback and deploy:cleanup will "destroy" the deploy directory :)
set :releases, lambda { |context|
  r = context.capture(:find, "#{context.releases_path} -mindepth 1 -maxdepth 1 -prune -type d")
  r.split.sort.map { |dir| File.basename(dir) }
}

# Load project configuration
self.extend Gosign::Config
loadConfig()

# We need a reference to the capistrano stuff in our utility functions, so
# we set it here.
Gosign.cap = self
