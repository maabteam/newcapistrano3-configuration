# Namespace for local tasks
namespace :local do

  # Set local permissions to 644 (files) and 755 (directories)
  # Also make *.phpsh scripts executable (775)
  task :permissions do
    cwd = Dir.pwd + "/"

    if not Gosign::Structure.correct?
      Gosign::Util.error("The correct directory structure is not yet set up, please run 'local:setup'")
      exit
    end

    Gosign::Perms.set(cwd + "../", true, { chmod: 644, type: "f" })
    Gosign::Perms.set(cwd + "../", true, { chmod: 755, type: "d" })
    Gosign::Perms.set(cwd + "../", true, { chmod: 775, type: "f", name: "*.phpsh" })
  end


  task :clean_config do
    Gosign::Util.exec("Cleaning local capistrano configuration...") do
      <<-eos
        git rm -r --cached config/capistrano/deploy.rb config/capistrano/gosign && \\
          git commit -m 'Automatically cleaned up Capistrano configuration.'
      eos
    end
  end
end
