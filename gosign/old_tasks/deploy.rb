namespace :deploy do

  # Synchronizes the remote user_uploads/ folder with the local one. Note that
  # the synchronization is non-destructive, that is, newer files on the remote
  # system won't be overwritten and locally deleted files won't be deleted
  # remotely.
  task :user_upload do
    source = Dir.pwd + "/fileadmin/user_upload/"
    target = deploy_to + "/data/fileadmin/user_upload/"
    roles[:app].servers.each do |server|
      Gosign::Util.exec("Transmitting local user_uploads/ to #{server}:") do
        "rsync -auxzv #{source} #{server}:#{target}"
      end
    end
  end

  # For TYPO3 6.0+, the caches now reside inside the typo3temp folder, which makes it necessary
  # to delete these files on deploy. They're defined in the main `deploy.rb` file, as an array
  # of paths relative to the TYPO3 root.
  task :clear_caches do
    fetch(:cache_locations).each do |cache|
      run "rm -rf #{latest_release}/#{cache}"
    end
  end

  task :run_unittests do
    fetch(:run_unittests).each do |test_path|
      logger.info "Running unit tests for #{test_path} in #{latest_release}"
      run "#{latest_release}/typo3/cli_dispatch.phpsh phpunit #{latest_release}/#{test_path}"
    end
  end
end

before "deploy:create_symlink", "deploy:compile_compass"
before "deploy:create_symlink", "deploy:permissions"
before "deploy:create_symlink", "deploy:run_unittests"
after "deploy:create_symlink", "deploy:resymlink"
after "deploy:create_symlink", "deploy:clear_caches"
after "deploy:create_symlink", "deploy:cleanup"

after "deploy:user_upload", "setup:set_data_permissions"
