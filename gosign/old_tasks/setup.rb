namespace :setup do

    task :set_data_permissions do
      Gosign::Perms.set(deploy_to + "/data/", false, { chmod: filePermissions, type: "f", chgrp: group, chown: owner })
      Gosign::Perms.set(deploy_to + "/data/", false, { chmod: setDirectoryPermissions, type: "d", chgrp: group, chown: owner })
      Gosign::Perms.set(deploy_to + "/data/", false, { chmod: phpshPermissions, type: "f", name: "*.phpsh", chgrp: group, chown: owner })
    end

end

before "deploy:setup", "setup:check_existing_structure"
after "deploy:setup", "setup:create_structure", "setup:extra_symlinks", "setup:set_data_permissions"
