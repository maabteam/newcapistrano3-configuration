require 'pathname'

class Gosign

  # Creates a directory hierarchy from a string representation. Hierarchy is determined
  # by indentation level. For example:
  #
  # foo
  #   bar -> /home/foo
  #   baz
  #     moep
  # test
  #
  # Creates the following directory structure, where `bar` is a symlink to `/home/foo`:
  #
  # o
  # |-- foo
  # |   |-- bar
  # |   `-- baz
  # |       `-- moep
  # `-- test
  #
  class DirectoryStructure
    def initialize(structure, tabwidth)
      @structure = structure.split("\n").reject{|e| e.strip.empty?}
      @tabwidth = tabwidth
      @root = { name: "root", type: :root, indentation: 0, children: [] }
      @stack = [ @root ]

      @mkdir = lambda { |dir| FileUtils.mkdir(dir) }
      @ln_s = lambda { |target, directory| FileUtils.ln_s(target, directory) }
    end

    def create_at(root, element=nil)
      element ||= treeify
      dir = Pathname.new("#{root}/#{element[:name]}").cleanpath.to_s

      if element[:type] == :root
        dir = Pathname.new("#{root}").cleanpath.to_s
      elsif element[:type] == :directory
        @mkdir.call(dir)
      elsif element[:type] == :symlink
        @ln_s.call(element[:target], dir)
      end

      if !dir.nil?
        element[:children].each { |x| create_at(dir, x) }
      end
    end

    def treeify
      previous = @root
      @structure.map do |line|
        indent = indentation(line)

        if previous[:indentation] < indent
          @stack.push(previous)
        elsif previous[:indentation] > indent
          @stack.pop
        end

        previous = parse_line(line.strip, indent)
        @stack.last[:children].push(previous)
      end

      @root
    end

    def mkdir=(proc)
      raise "Parameter must be callable" if !proc.respond_to? :call
      @mkdir = proc
    end

    def ln_s=(proc)
      raise "Parameter must be callable" if !proc.respond_to? :call
      @ln_s = proc
    end

    def parse_line(line, indent)
      item = { name: "", type: nil, indentation: indent, children: [] }

      case line
      when /^(.+)->(.+)$/
        item[:name] = $1.strip
        item[:target] = $2.strip
        item[:type] = :symlink
      else
        item[:name] = line
        item[:type] = :directory
      end

      item
    end

    def indentation(line)
      indent = /^(\s*)/.match(line)[0]

      if (indent.length % @tabwidth) != 0
        raise "Indentation was not an even multiple of the tabwidth on '#{line}'"
      end

      if !indent.index(" ").nil? && !indent.index("\t").nil?
        raise "Mixed indentation (tabs and spaces) is not allowed on '#{line}'"
      end

      indent.length
    end
  end
end
