module SSHKit

  module Formatter

    class Shouting < SSHKit::Formatter::Pretty

      def format_with_banner(message)
        banner = "#" * (message.length)

        <<-eof
      #{banner}
      #{message}
      #{banner}
eof
      end

      def format_message(verbosity, message, uuid=nil)
        message = "[#{colorize(uuid, :green)}] #{message}" unless uuid.nil?
        level = colorize(Pretty::LEVEL_NAMES[verbosity], Pretty::LEVEL_COLORS[verbosity])
        output = '%6s %s' % [level, message]

        if verbosity < 2 # INFO and DEBUG do not get a banner
          output
        else
          format_with_banner(output)
        end
      end

    end

  end

end
