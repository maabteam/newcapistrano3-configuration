class Gosign
  module Util

    PRE_TYPO3_6_0 = 1
    TYPO3_6_0 = 2
    TYPO3_6_2 = 4
    TYPO3_7_6 = 8
    def self.project_typo3_version
      packageStates = "typo3conf/PackageStates.php"
      localConfiguration = "typo3conf/LocalConfiguration.php"
      localconf = "typo3conf/localconf.php"
      typo3_7_6 = "typo3_7_6.php"

      if File.symlink?(typo3_7_6) || File.exists?(typo3_7_6)
        TYPO3_7_6
      elsif File.symlink?(packageStates) || File.exists?(packageStates)
        TYPO3_6_2 | TYPO3_6_0
      elsif File.symlink?(localConfiguration) || File.exists?(localConfiguration)
        TYPO3_6_0
      elsif File.symlink?(localconf) || File.exists?(localconf)
        PRE_TYPO3_6_0
      else
        abort "Couldn't determine TYPO3 Version. Aborting...!"
      end
    end


    # Creates a mysqldump command that dumps table structure of all tables
    #
    def self.mysqldumpStructure(dbName)
      cmd = "mysqldump --default-character-set=utf8 --no-data #{dbName} "

      cmd
    end

    # Creates a mysqldump command that dumps table data but does not dump Typo3 temporary tables
    #
    def self.mysqldumpData(dbName)
      blacklist = [
        "be_sessions",
        "cache_extensions",
        "cache_hash",
        "cache_imagesizes",
        "cache_md5params",
        "cache_pages",
        "cache_pagesection",
        "cache_sys_dmail_stat",
        "cache_treelist",
        "cache_typo3temp_log",
        "cf_cache_hash",
        "cf_cache_hash_tags",
        "cf_cache_pages",
        "cf_cache_pages_tags",
        "cf_cache_pagesection",
        "cf_cache_pagesection_tags",
        "cf_cache_rootline",
        "cf_cache_rootline_tags",
        "cf_extbase_datamapfactory_datamap",
        "cf_extbase_datamapfactory_datamap_tags",
        "cf_extbase_object",
        "cf_extbase_object_tags",
        "cf_extbase_reflection",
        "cf_extbase_reflection_tags",
        "cf_extbase_typo3dbbackend_tablecolumns",
        "cf_extbase_typo3dbbackend_tablecolumns_tags",
        "fe_session_data",
        "fe_sessions",
        "sys_log",
        "tx_realurl_chashcache",
        "tx_realurl_errorlog",
        "tx_realurl_pathcache",
        "tx_realurl_urldecodecache",
        "tx_realurl_urlencodecache"
      ]

      cmd = "mysqldump --skip-lock-tables --default-character-set=utf8 --no-create-info #{dbName} "
      blacklist.each { |table| cmd += "--ignore-table=#{dbName}.#{table} " }

      cmd
    end


    # This method will convert any string into a string that can be used for a filename
    def self.filename_friendly(string)
      string.gsub(/[^\w\s_-]+/, '')
            .gsub(/(^|\b\s)\s+($|\s?\b)/, '\\1\\2')
            .gsub(/\s+/, '_')
    end


    def self.php_cmd_check_extension_active(release_path, ext_name)
      cmd = <<-eof
$x = include("#{release_path}/typo3conf/PackageStates.php");
if(!isset($x["packages"]["#{ext_name}"])) {
    exit(1);
}
$x["packages"]["#{ext_name}"]["state"] === "active" ? exit(0) : exit(1);
      eof
    end
  end
end
