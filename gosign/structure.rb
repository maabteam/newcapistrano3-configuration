# Project structure setup logic
class Gosign
  class Structure

    if Gosign::Util.project_typo3_version & Gosign::Util::TYPO3_7_6 > 0
      sourceDir = 'src -> ../source7'
      templatesDir = ''
    elsif Gosign::Util.project_typo3_version & Gosign::Util::TYPO3_6_2 > 0
      sourceDir = 'src -> ../source6-2'
      templatesDir = ''
    elsif Gosign::Util.project_typo3_version & Gosign::Util::TYPO3_6_0 > 0
      sourceDir = 'src -> ../source6'
      templatesDir = 'templates -> ../../htdocs/templates'
    else
      sourceDir = 'src -> ../Typo3Source'
      templatesDir = 'templates -> ../../htdocs/templates'
    end

    DEFAULT = <<-eof
data/
  fileadmin/
    user_upload/
    #{templatesDir}
  l10n/
  typo3temp/
  uploads/
local/
#{sourceDir}
eof

    # Sets up our default project structure
    def self.setup(context, path)
      x = DirectoryStructure.new(DEFAULT, 2)

      directories = []
      symlinks = []
      x.mkdir = lambda { |dir| directories.push(dir) }
      x.ln_s = lambda { |target, directory|  symlinks.push([target, directory]) }

      x.create_at(path)

      context.execute :mkdir, "-v #{directories.join(' ')}"
      symlinks.each do |link|
        context.execute :ln, "-s #{link[0]} #{link[1]}"
      end

      # Create a .htaccess file to save the TYPO3 application context as ENV in it
      context.execute :echo, "'\n# set TYPO3 application context\nSetEnv TYPO3_CONTEXT #{fetch :applicationContext}\n' >> #{path}.htaccess"
    end

    # Copy sample files into the local directory
    def self.copySampleFiles(context, path_from, path_to)
      # Copy all sample configuration files from htdocs/config/local.sample/ to local/
      sampleFiles = context.capture(:find, "#{path_from}config/local.sample/ -mindepth 1 -maxdepth 1 -prune \\( -type f -or -type l \\) -name *.sample")
      sampleFiles.split.each do |file|
        newFile = "#{path_to}local/" + File.basename(file, '.sample')
        context.execute :cp, "-a #{file} #{newFile}"
      end
    end

  end
end
