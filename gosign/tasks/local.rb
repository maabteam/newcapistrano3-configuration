# Tasks that are run locally, not on the server
namespace :local do

  desc "Sets up the correct project structure after cloning a project"
  task :setup do
    run_locally do

      # The TYPO3 application context
      # Set to 'Development' for local:setup
      # Is written into a .htaccess file while setup
      set :applicationContext, "Development"

      if test("[ -d '#{Dir.pwd}/../local' ]")
        info "Project structure seems to be set up. Not re-creating."
      else
        info "Setting up default project structure"
        execute(:mkdir, "htdocs")
        execute(:find, ". -mindepth 1 -maxdepth 1 -prune -exec mv {} htdocs/ \\;")
        Gosign::Structure.setup(self, "#{Dir.pwd}/")
        Gosign::Structure.copySampleFiles(self, "#{Dir.pwd}/htdocs/", "#{Dir.pwd}/")
      end

    end
  end

  desc "Watches the configured Compass directory to generate derived CSS files"
  task :compass do
    run_locally do
      if !test("which compass")
        error "You don't seem to have compass installed, or it's not in your PATH :("
        exit 1
      end

      exec "cd #{fetch :compass_directory} && compass watch"
    end
  end

end
