namespace :deploy do

  # Create a symlink from htdocs to current to keep our DefaultProjekt structure intact.
  task :resymlink do
    on release_roles :all do |host|
      relative_path = Pathname.new(current_path).relative_path_from(Pathname.new(deploy_to))
      symlinks = [
        # [ target, directory ]
        [relative_path, "htdocs"],
        ["../data", "releases/data"],
        ["../src", "releases/src"],
        ["../local", "releases/local"]
      ]

      symlinks.each do |link|
        dir = "#{deploy_to}/#{link[1]}"
        if test("[ ! -h #{dir} ]")
          execute :ln, "-s #{link[0]} #{dir}"
        end
      end

    end
  end

  task :permissions do
    on release_roles :all do |host|
      cmds = []
      cmds += Gosign::Perms.set(release_path, false, { chgrp: fetch(:group), chown: fetch(:owner) })
      cmds += Gosign::Perms.set(deploy_to + "/local/", false, { chgrp: fetch(:group), chown: fetch(:owner) })

      cmds += Gosign::Perms.set(release_path, false, { chmod: fetch(:filePermissions), type: "f" })
      cmds += Gosign::Perms.set(release_path, false, { chmod: fetch(:setDirectoryPermissions), type: "d" })
      cmds += Gosign::Perms.set(release_path, false, { chmod: fetch(:phpshPermissions), type: "f", name: "*.phpsh" })
      cmds += Gosign::Perms.set(deploy_to + "/local/", false, { chmod: fetch(:filePermissions), type: "f" })
      cmds += Gosign::Perms.set(deploy_to + "/local/", false, { chmod: fetch(:setDirectoryPermissions), type: "d" })

      cmds.each { |cmd| execute *cmd }
    end
  end

  # Allow deploy-time compilation of Compass assets. Can be enabled in project.rb
  task :compile_compass do
    on release_roles :all do |host|
      if fetch(:enable_compass)
        compass = fetch(:compass_path) || 'compass'
        compass_cmd = "#{compass} compile"
        compass_cmd += " -e production" if fetch(:compass_minify)

        if !test("source /etc/profile; which #{compass}")
          error "Could not find compass on remote server. Cannot compile SCSS."
          exit 1
        end

        info "Compiling compass projects in #{fetch :release_path}..."
        execute "source /etc/profile; for dir in $(find #{fetch :release_path} -name config.rb); do if ! #{compass_cmd} $(dirname $dir); then exit 1; fi; done"
      end
    end
  end

  # For TYPO3 6.0+, the caches now reside inside the typo3temp folder, which makes it necessary
  # to delete these files on deploy. They're defined in the main `deploy.rb` file, as an array
  # of paths relative to the TYPO3 root.
  task :clear_caches do
    on release_roles :all do |host|
      fetch(:cache_locations).each do |cache|
        execute :rm, '-rf', "#{release_path}/#{cache}"
      end
      if !test("[ -d '#{release_path}/typo3conf/ext/coreapi' ]") || !test("php -r '#{Gosign::Util.php_cmd_check_extension_active(release_path, 'coreapi')}'")
        warn "Could not find extension 'coreapi', could not clear the cache properly"
      else
        execute :bash, "-c '#{release_path}/typo3/cli_dispatch.phpsh extbase cacheapi:clearallcaches'"
      end
    end
  end

  task :database_compare do
    on release_roles :all do |host|
      if !test("[ -d '#{release_path}/typo3conf/ext/coreapi' ]") || !test("php -r '#{Gosign::Util.php_cmd_check_extension_active(release_path, 'coreapi')}'")
        warn "Database could not be compared because coreapi is not installed!"
      else
        # The numbers at the end of the command are identifiers for the actions (don't look
        # at me like that, I did not come up with this :D)
        execute :bash, "-c '#{release_path}/typo3/cli_dispatch.phpsh extbase databaseapi:databasecompare 4'" # ACTION_UPDATE_CREATE_TABLE
        execute :bash, "-c '#{release_path}/typo3/cli_dispatch.phpsh extbase databaseapi:databasecompare 2'" # ACTION_UPDATE_ADD
        execute :bash, "-c '#{release_path}/typo3/cli_dispatch.phpsh extbase databaseapi:databasecompare 3'" # ACTION_UPDATE_CHANGE
      end
    end
  end

  namespace :check do

    # Checks whether or not the project structure was already created, and creates it if not.
    desc 'Gosign specific project structure creation'
    task :structure do
      on release_roles :all do |host|
        if test("[ -d '#{deploy_to}/local' ]")
          info "Project structure seems to be set up. Not re-creating."
          set :do_setup, false;
        else
          info "Setting up default project structure"
          set :do_setup, true;
          Gosign::Structure.setup(self, "#{deploy_to}/")
        end
      end
    end

    desc 'Gosign specific local configuration files'
    task :local_files do
      on release_roles :all do |host|
        if fetch(:do_setup)
          info "Copy local configuration files from sample folder"
          Gosign::Structure.copySampleFiles(self, "#{release_path}/", "#{deploy_to}/")
        end
      end
    end

  end

  Rake::Task['deploy:symlink:release'].clear_actions
  namespace :symlink do
    desc 'Symlink release to current (relative)'
    task :release do
      on release_roles :all do |host|
        rel_release_path = Pathname.new(release_path).relative_path_from(Pathname.new(deploy_to))

        execute :rm, '-rf', current_path
        execute :ln, '-s', rel_release_path, current_path
      end
    end
  end
end

after "deploy:check", "deploy:check:structure"
before "deploy:publishing", "deploy:permissions"
before "deploy:publishing", "deploy:compile_compass"
before "deploy:publishing", "deploy:resymlink"
before "deploy:publishing", "deploy:check:local_files"
after "deploy:publishing", "deploy:clear_caches"
after "deploy:publishing", "deploy:database_compare"

