namespace :remote do

  desc "Displays the stage's uname. For testing purpose."
  task :uname do
    on release_roles :all do |host|
      execute :uname, '-a'
    end
  end

  desc "Opens an SSH session in the 'deploy_to' directory of the target server."
  task :ssh do
    run_locally do
      server = Capistrano::Configuration.env.primary(:app)
      if server.nil?
        error "Cannot open SSH session. Could not find server for role 'app'."
        error "Please check your stage configuration."
        exit 1
      end

      command = "ssh -t #{server.user}@#{server.hostname} \"cd #{fetch(:deploy_to)} && bash --login\""
      info "Executing #{command}"
      exec command
    end
  end

  namespace :db do

    desc "Dump the target's TYPO3 database in a file and download it to the project root."
    task :download do
      on release_roles :all do |host|
        typo3version = Gosign::Util.project_typo3_version
        if typo3version == Gosign::Util::PRE_TYPO3_6_0

          phpCmd = <<-PHP
error_reporting(E_ERROR);
define("PATH_site", getcwd() . "/htdocs/");
require "local/localconf.php";
echo "-DB-".PHP_EOL.$typo_db.PHP_EOL.$typo_db_username.PHP_EOL.$typo_db_password.PHP_EOL.$typo_db_host;
          PHP

        elsif typo3version == Gosign::Util::TYPO3_6_0
          phpCmd = <<-PHP
error_reporting(E_ERROR);
$configArray = require "local/LocalConfiguration.php";
echo "-DB-".PHP_EOL.$configArray['DB']['database'].PHP_EOL.$configArray['DB']['username'].PHP_EOL.$configArray['DB']['password'].PHP_EOL.$configArray['DB']['host'];
          PHP

        elsif typo3version & Gosign::Util::TYPO3_6_2
          phpCmd = <<-PHP
error_reporting(E_ERROR);
$configFile = file_get_contents("local/AdditionalConfiguration.php");
foreach (explode(PHP_EOL, $configFile) as $line) {
  if (strpos($line, 'TYPO3_CONF_VARS') !== FALSE && strpos($line, 'DB') !== FALSE) {
    eval($line);
  }
}
$configArray = $GLOBALS['TYPO3_CONF_VARS']['DB'];
echo "-DB-".PHP_EOL.$configArray['database'].PHP_EOL.$configArray['username'].PHP_EOL.$configArray['password'].PHP_EOL.$configArray['host'];
          PHP

        else
          abort "DB download not configured for actual TYPO3 Version. Aborting...!"
        end

        phpCmd.gsub!("\n", ' ')
        getDatabaseAccessCmd = "cd #{deploy_to}; php -r '#{phpCmd}'"

        databaseAccessData = capture(getDatabaseAccessCmd)
        databaseAccessData = databaseAccessData.split("\n")

        # Indicates where our data begins, because there may be
        # PHP warnings before it, which cannot be disabled
        start = databaseAccessData.index('-DB-')

        db = databaseAccessData[start+1]
        dbuser = databaseAccessData[start+2]
        pwd = databaseAccessData[start+3]
        host = databaseAccessData[start+4]

        # Dump the database into a temporary file (gzipped)
        mysqlHostUserPassword = "-h #{host} -u #{dbuser} --password='#{pwd}'"

        mysqldumpStructureCmd = Gosign::Util.mysqldumpStructure(db) + mysqlHostUserPassword
        mysqldumpDataCmd = Gosign::Util.mysqldumpData(db) + mysqlHostUserPassword

        mysqlCmd = "TMPFILE=`mktemp` && #{mysqldumpStructureCmd} | gzip > $TMPFILE && #{mysqldumpDataCmd} | gzip >> $TMPFILE && echo $TMPFILE"
        mysqlCmdResult = capture(mysqlCmd)

        tempFile = mysqlCmdResult.gsub("\n", '')
        mysqlDumpFilename = "#{Dir.pwd}/#{fetch :application}_#{fetch :stage}_#{Gosign::Util.filename_friendly(Time.now.to_s)}_mysql.dump.gz"

        download!(tempFile, mysqlDumpFilename)
        execute :rm, tempFile
      end
    end

  end

  namespace :scp do
    desc "Download files from 'deploy_to' data/ directory of the target server into the local data directory."
    task :download do
      run_locally do
        server = Capistrano::Configuration.env.primary(:app)
        if server.nil?
          error "Cannot open SSH session for secure copy. Could not find server for role 'app'."
          error "Please check your stage configuration."
          exit 1
        end

        if ENV['path'].nil?
          error "You have to set the path to download (path=<PATH>) relative to the data directory"
          exit 1
        end

        set :remotePath, "data/#{ENV['path']}"

        set :localPath, "../data/#{File.dirname(ENV['path'])}"

        if !File.exists?(fetch(:localPath))
           system 'mkdir', '-p', fetch(:localPath)
        end

        command = "scp -prP #{fetch(:scpPort, 46022)} #{server.user}@#{server.hostname}:#{fetch(:deploy_to)}/#{fetch(:remotePath)} #{fetch(:localPath)}"
        info "Executing #{command}"
        exec command

      end
    end
  end
end
