require 'rubygems'
require 'fileutils'

# This Capfile bootstrap the capistrano configuration from the git repository.

# ==============================
# Get the capistrano config repo
# ==============================

begin
  require 'not-quite-submodules'
rescue LoadError
  puts "\x1b[41mPlease run 'gem install not-quite-submodules'\x1b[0m"
  exit 1
end

NotQuiteSubmodules.initialize(
 "git@git.gosign.de:miscellaneous/capistrano3-configuration.git",
 "config/capistrano/",

 # For now, the update interval must be one second, since the update mechanism is still a
 # bit buggy.
 :update_interval => 1,

 # Activate only for testing purpose
 # :force_update_to => "origin/master"
)
